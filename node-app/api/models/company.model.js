const mongoose = require('mongoose');

const CompanySchema = new mongoose.Schema({
    name: {
        required: true,
        type: String
    },
    city: String,
    address: String
});

module.exports = mongoose.model('Company', CompanySchema);
