const companyController = require("../controllers/company.controller");

module.exports = [
    {
        path:"/api/companies",
        method:'POST',
        handler:companyController.create
    },
    {
       path:"/api/companies",
       method: 'GET',
       handler:companyController.find
    },
    {
       path:"/api/companies/{id}",
       method: 'GET',
       handler:companyController.findOne
    },
    {
        path:"/api/companies/{id}",
        method:'DELETE',
        handler:companyController.delete
    },
    {
        path:"/api/companies/{id}",
        method:'PUT',
        handler:companyController.update
    }
]
