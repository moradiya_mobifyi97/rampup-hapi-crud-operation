const hapi = require('hapi');
const server = new hapi.Server();
const mongoose = require('mongoose');
const mongoDbUri = 'mongodb://localhost:27017/node_app';
const companyRouter = require("./api/routes/company.routes");

//connect with mongoDB
mongoose.connect(mongoDbUri, {useNewUrlParser: true}, (err) => {
    if(!err){
        console.log("mongoDB connection succeeded");
    }else{
        console.log("mongodb connection faild")
    }
});
// server port
server.connection({host: 'localhost', port: '3000'});

// Router
server.route(companyRouter);

// start the Hapi server on your localhost
server.start(err => {
    if (err) {
        throw err;
    }
    console.log(`Server Running at PORT ${server.info.port}`);
}) 
 